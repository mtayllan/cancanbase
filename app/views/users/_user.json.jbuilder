json.extract! user, :id, :name, :position, :information, :created_at, :updated_at
json.url user_url(user, format: :json)
