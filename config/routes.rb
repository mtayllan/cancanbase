Rails.application.routes.draw do
  resources :users
  resources :activities
  resources :inventories
  resources :recipes
  resources :admins
  devise_for :admins, controllers: {
    confirmations: 'admins/confirmations',
    passwords: 'admins/passwords',
    registrations: 'admins/registrations',
    sessions: 'admins/sessions',
    unlocks: 'admins/unlocks',  
  }

  resources :roles

  root 'application#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
